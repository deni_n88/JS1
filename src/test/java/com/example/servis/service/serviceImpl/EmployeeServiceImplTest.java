package com.example.servis.service.serviceImpl;

import com.example.servis.model.Employee;
import com.example.servis.repository.EmployeeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTest {

    @Mock
    private EmployeeRepository employeeRepositoryMock;

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Test
    public void getEmployeeTest() {
        Employee newEmployee = new Employee();
        newEmployee.setId(2L);
        doReturn(newEmployee).when(employeeRepositoryMock).getOne(ArgumentMatchers.anyLong());

        Employee result = employeeService.getEmployee(2L);
        assertEquals(2L, result.getId().longValue());
    }

    @Test
    public void createEmployeeTest() {

        Employee newEmployee = new Employee("employee");
        doReturn(newEmployee).when(employeeRepositoryMock).save(new Employee(ArgumentMatchers.any()));

        Employee result = employeeService.createEmployee("employee");
        assertEquals("employee", result.getName());
    }

    @Test
    public void deleteEmployeeTest() {

        final Employee employee = new Employee("employee");
        employee.setId(1L);

        doReturn(employee).when(employeeRepositoryMock).getOne(ArgumentMatchers.anyLong());

        final boolean result = employeeService.deleteEmployee(1L);
        verify(employeeRepositoryMock, times(2)).getOne(ArgumentMatchers.anyLong());
        verify(employeeRepositoryMock, times(1)).deleteById(1L);
        assertFalse(result);
    }

    @Test
    public void findAllTest() {
        Employee employee = new Employee();
        Employee employee1 = new Employee();
        Iterable<Employee> employeeArrayList = new ArrayList<>();
        ((ArrayList<Employee>) employeeArrayList).add(employee);
        ((ArrayList<Employee>) employeeArrayList).add(employee1);
        doReturn(employeeArrayList).when(employeeRepositoryMock).findAll();
        Iterable<Employee> findAllList = employeeService.findAll();

        assertEquals(2L, ((ArrayList<Employee>) findAllList).size());
    }
}