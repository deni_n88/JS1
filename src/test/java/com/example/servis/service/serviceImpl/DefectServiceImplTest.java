package com.example.servis.service.serviceImpl;

import com.example.servis.model.Car;
import com.example.servis.model.Defect;
import com.example.servis.model.Employee;
import com.example.servis.repository.DefectRepository;
import com.example.servis.service.CarService;
import com.example.servis.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class DefectServiceImplTest {

    @Mock
    private DefectRepository defectRepositoryMock;

    @Mock
    private EmployeeService employeeServiceMock;

    @Mock
    private CarService carServiceMock;

    @InjectMocks
    private DefectServiceImpl defectService;

    @Test
    public void getDefect() {
        doReturn(new Defect("defect", 12, new Car())).when(defectRepositoryMock).getOne(ArgumentMatchers.anyLong());

        Defect result = defectService.getDefect(1L);

        assertEquals("defect", result.getDetails());

        verify(defectRepositoryMock).getOne(1L);

    }

    @Test
    public void getCarByDefect() {
        Defect newDefect = new Defect();
        newDefect.setCar(new Car("carModel", "A12-A-123"));
        doReturn(newDefect).when(defectRepositoryMock).getOne(ArgumentMatchers.anyLong());
        Car result = defectService.getCarByDefect(1L);

        assertEquals("carModel", result.getCarModel());
    }

    @Test
    public void defectList() {

        Iterable<Defect> defectArrayList = new ArrayList<>();
        ((ArrayList<Defect>) defectArrayList).add(new Defect());
        doReturn(defectArrayList).when(defectRepositoryMock).findAll();

        List<Defect> arrayList = defectRepositoryMock.findAll();

        assertEquals(1, ((arrayList).size()));

        verify(defectRepositoryMock).findAll();

    }

    @Test
    public void createDefect() {

        Car newCar = new Car("carModel", "A12-A-123");
        doReturn(newCar).when(carServiceMock).getCar(ArgumentMatchers.anyLong());
        Employee newEmployee = new Employee("employee");
        doReturn(newEmployee).when(employeeServiceMock).getEmployee(ArgumentMatchers.anyLong());
        Defect newDefect = new Defect("defect", 10, newCar);
        doReturn(newDefect).when(defectRepositoryMock).save(ArgumentMatchers.any());
        Defect result = defectService.createDefect(1L, 1L, newDefect);

        assertEquals("defect", result.getDetails());
    }
}