package com.example.servis.service.serviceImpl;

import com.example.servis.model.Car;
import com.example.servis.model.Employee;
import com.example.servis.repository.CarRepository;
import com.example.servis.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class CarServiceImplTest {

    @Mock
    private CarRepository carRepositoryMock;

    @Mock
    private EmployeeService employeeServiceMock;

    @Captor
    private ArgumentCaptor<Car> carArgumentCaptor;

    @InjectMocks
    private CarServiceImpl actualService;

    @Test
    public void carListTest() {

        Iterable<Car> carlist = new ArrayList<>();
        ((ArrayList<Car>) carlist).add(new Car());
        doReturn(carlist).when(carRepositoryMock).findAll();

        Iterable<Car> findAll = actualService.carList();

        assertEquals(1, ((ArrayList<Car>) findAll).size());
    }

    @Test
    public void createCarTest() {
        doReturn(new Employee()).when(employeeServiceMock).getEmployee(ArgumentMatchers.anyLong());
        Car savedCar = new Car();
        savedCar.setId(5L);
        doReturn(savedCar).when(carRepositoryMock).save(ArgumentMatchers.any());

        Car result = actualService.createCar(2L, new Car());

        assertEquals(5L, result.getId().longValue());

        verify(carRepositoryMock).save(carArgumentCaptor.capture());
        assertEquals(1, carArgumentCaptor.getValue().getEmployees().size());
    }


    @Test
    public void editCarTest() {
        doReturn(new Car("OldCarModel", "O12-O-123")).when(carRepositoryMock).getOne(1L);

        doReturn(new Car("NewCarModel", "A12-A-123")).when(carRepositoryMock).save(ArgumentMatchers.any());

        Car result = actualService.editCar(1L, new Car("CarModel", "A12-A-123"));


        assertEquals("NewCarModel", result.getCarModel());
    }

    @Test
    public void getCarTest() {
        doReturn(new Car("carModel", "A12-A-123")).when(carRepositoryMock).getOne(ArgumentMatchers.anyLong());

        Car result = actualService.getCar(1L);

        assertEquals("carModel", result.getCarModel());
    }

    @Test
    public void deleteCarTest() {
        actualService.deleteCar(1L);
        verify(carRepositoryMock).deleteById(1L);
    }

    @Test
    public void findByCarModelTest() {
        actualService.findByCarModel("carModel");
        verify(carRepositoryMock).findByCarModelContains("carModel");
    }


    @Test
    public void getByLicencePlate() {
        doReturn(new Car("carModel", "A12-A-123")).when(carRepositoryMock).getByLicencePlate(ArgumentMatchers.anyString());

        Car result = actualService.getByLicencePlate("A12-A-123");

        assertEquals("A12-A-123", result.getLicencePlate());

    }
}