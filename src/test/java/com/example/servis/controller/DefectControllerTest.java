package com.example.servis.controller;

import com.example.servis.model.Defect;
import com.example.servis.service.DefectService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(MockitoJUnitRunner.class)
public class DefectControllerTest {

    private String rootUrl = "/defects";

    private MockMvc mockMvc;

    @Mock
    private DefectService defectServiceMock;

    @InjectMocks
    private DefectController defectController;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(defectController).build();
    }

    @Test
    public void defectListTest() throws Exception {

        mockMvc.perform(
                get(rootUrl)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(defectServiceMock).defectList();
    }

    @Test
    public void getByDefectTest() throws Exception {
        mockMvc.perform(
                get(rootUrl + "/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(defectServiceMock).getCarByDefect(1L);
    }

    @Test
    public void saveDefectTest() throws Exception {

        Defect defect = new Defect();
        defect.setCost(30f);
        defect.setDetails("details");

        ObjectMapper om = new ObjectMapper();
        om.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(defect);

        mockMvc.perform(
                post(rootUrl + "/post/1/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(defectServiceMock).createDefect(1L, 1L, defect);
    }
}