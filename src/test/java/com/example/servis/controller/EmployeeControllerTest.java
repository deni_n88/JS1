package com.example.servis.controller;

import com.example.servis.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeControllerTest {

    private String rootUrl = "/employees";


    private MockMvc mockMvc;

    @Mock
    private EmployeeService employeeServiceMock;

    @InjectMocks
    private EmployeeController employeeControllerMock;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(employeeControllerMock).build();
    }


    @Test
    public void employeeListTest() throws Exception {

        mockMvc.perform(
                get(rootUrl)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(employeeServiceMock).findAll();
    }

    @Test
    public void getEmployeeTest() throws Exception {

        mockMvc.perform(
                get(rootUrl + "/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(employeeServiceMock).getEmployee(1L);
    }

    @Test
    public void createEmployeeTest() throws Exception {
        mockMvc.perform(
                post(rootUrl + "/post/deni")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(employeeServiceMock).createEmployee("deni");
    }

    @Test
    public void deleteEmployeeTest() throws Exception {
        mockMvc.perform(
                delete(rootUrl + "/delete/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(employeeServiceMock).deleteEmployee(1L);
    }
}