package com.example.servis.controller;

import com.example.servis.model.Car;
import com.example.servis.service.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(MockitoJUnitRunner.class)
public class CarControllerTest {

    private String rootUrl = "/cars";

    private MockMvc mockMvc;

    @Mock
    private CarService carServiceMock;

    @InjectMocks
    private CarController carControllerMock;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(carControllerMock).build();
    }

    @Test
    public void getCarTest() throws Exception {
        mockMvc.perform(
                get(rootUrl + "/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(carServiceMock).getCar(1L);
    }

    @Test
    public void carListTest() throws Exception {
        mockMvc.perform(
                get(rootUrl)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(carServiceMock).carList();
    }

    @Test
    public void findByCarModelTest() throws Exception {
        mockMvc.perform(
                get(rootUrl + "/find/car")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(carServiceMock).findByCarModel("car");
    }

    @Test
    public void getByLicencePlateTest() throws Exception {
        mockMvc.perform(
                get(rootUrl + "/find/licencePlate/A23-A-123")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(carServiceMock).getByLicencePlate("A23-A-123");
    }

    @Test
    public void postCarTest() throws Exception {
        Car car = new Car();
        car.setId(1L);
        car.setCarModel("Auto");
        car.setLicencePlate("A23-A-123");

        ObjectMapper om = new ObjectMapper();
        om.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(car);
        mockMvc.perform(
                post(rootUrl + "/post/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(carServiceMock).createCar(1L, car);
    }

    @Test
    public void editCarTest() throws Exception {
        Car car = new Car();
        car.setId(1L);
        car.setCarModel("Auto");
        car.setLicencePlate("A23-A-123");

        ObjectMapper om = new ObjectMapper();
        om.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(car);
        mockMvc.perform(
                put(rootUrl + "/edit/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson))
                .andExpect(MockMvcResultMatchers.status().isOk());
        verify(carServiceMock).editCar(1L, car);
    }

    @Test
    public void delCarTest() throws Exception {
        mockMvc.perform(
                delete(rootUrl + "/delete/2")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(carServiceMock).deleteCar(2L);
    }
}
