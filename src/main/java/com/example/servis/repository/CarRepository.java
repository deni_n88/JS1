package com.example.servis.repository;

import com.example.servis.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Iterable<Car> findByCarModelContains(String carModel);

    Car getByLicencePlate(String licencePlate);

    Car getCarByDefects(Long id);
}
