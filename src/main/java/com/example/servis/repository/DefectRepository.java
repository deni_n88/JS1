package com.example.servis.repository;

import com.example.servis.model.Defect;
import com.example.servis.model.Car;
import com.example.servis.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DefectRepository extends JpaRepository<Defect,Long> {


}
