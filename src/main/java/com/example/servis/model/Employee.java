package com.example.servis.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Table
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min = 3, max = 20)
    private String name;

    @ManyToMany(mappedBy = "employees")
    private List<Car> cars = new ArrayList<>();

    @ManyToMany(mappedBy = "employeeList")
    private List<Defect> defects = new ArrayList<>();

    public Employee() {
    }

    public Employee(String name, List<Car> cars, List<Defect> defects) {
        this.name = name;
        this.cars = cars;
        this.defects = defects;
    }

    public Employee(String name) {
        this.name = name;
    }

    public Employee(String name, List<Car> cars) {
        this.name = name;
        this.cars = cars;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Defect> getDefects() {
        return defects;
    }

    public void setDefects(List<Defect> defects) {
        this.defects = defects;
    }
}
