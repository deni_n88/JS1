package com.example.servis.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.NonNull;

@Table
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Defect {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min = 2, max = 30)
    private String details;

    @NonNull
    private Float cost;

    @ManyToMany()
    @JoinTable(name = "defect_employee", joinColumns = @JoinColumn(name = "id_defect", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "id_employee", referencedColumnName = "id"))
    @JsonIgnore
    private List<Employee> employeeList = new ArrayList<>();

    @ManyToOne()
    @JoinColumn(name = "cars_id")
    @JsonIgnore
    private Car car;

    public Defect() {
    }

    public Defect(String details, float cost, Car car) {
        this.details = details;
        this.cost = cost;
        this.car = car;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Defect defect = (Defect) o;
        return Objects.equals(details, defect.details) &&
            Objects.equals(cost, defect.cost) &&
            Objects.equals(employeeList, defect.employeeList) &&
            Objects.equals(car, defect.car);
    }

    @Override
    public int hashCode() {

        return Objects.hash(details, cost, employeeList, car);
    }

    @Override
    public String toString() {
        return id + ":" + cost + ":" + details;
    }
}
