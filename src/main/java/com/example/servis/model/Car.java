package com.example.servis.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.NonNull;

@Table
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Size(min = 2, max = 20)
    private String carModel;

    @NonNull
    private String licencePlate;

    @ManyToMany()
    @JoinTable(name = "car_employees",
        joinColumns = @JoinColumn(name = "car_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"))
    @JsonIgnore
    private List<Employee> employees = new ArrayList<>();

    public Car() {
    }

    public Car(String carModel, String licencePlate) {
        this.carModel = carModel;
        this.licencePlate = licencePlate;
    }

    public Car(String carModel, List<Employee> employees) {
        this.carModel = carModel;
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @OneToMany(mappedBy = "car")
    private List<Defect> defects;

    public List<Defect> getDefects() {
        return defects;
    }

    public void setDefects(List<Defect> defects) {
        this.defects = defects;
    }

    @Override
    public String toString() {
        return "id=" + id +
            ", carModel=" + carModel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }
        if (!(o instanceof Car)) {
            return false;
        }

        Car user = (Car) o;

        return user.carModel.equals(carModel) &&
            user.id == id &&
            user.licencePlate.equals(licencePlate);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + carModel.hashCode();
        result = 31 * result + id.intValue();
        result = 31 * result + licencePlate.hashCode();
        return result;
    }
}
