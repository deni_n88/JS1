package com.example.servis.controller;


import com.example.servis.model.Defect;
import com.example.servis.service.DefectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/defects")
public class DefectController {

    @Autowired
    private DefectService defectService;

    @GetMapping
    public Iterable<Defect> defectsList() {
        return this.defectService.defectsList();
    }

    @PostMapping("{carId}/{employeeId}")
    public Defect saveDefect(@PathVariable Long carId, @PathVariable Long employeeId, @RequestBody Defect defect) {
        return defectService.saveDefect(carId, employeeId, defect);
    }
}
