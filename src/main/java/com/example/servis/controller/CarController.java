package com.example.servis.controller;

import com.example.servis.model.Car;
import com.example.servis.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/cars")
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping()
    public Iterable<Car> carList() {
        return carService.carList();
    }

    @GetMapping("{id}")
    public Car getCar(@PathVariable Long id) {
        return carService.getCar(id);
    }

    @GetMapping("/carModel/{carModel}")
    public Iterable<Car> findByCarModel(@PathVariable String carModel) {
        return carService.findByCarModel(carModel);
    }

    @GetMapping("/licencePlate/{licencePlate}")
    public Car getByLicencePlate(@PathVariable String licencePlate) {
        return carService.getByLicencePlate(licencePlate);
    }

    @GetMapping("/defect/{defectId}")
    public Car getCarByDefectId(@PathVariable Long defectId) {
        return carService.getCarByDefectId(defectId);
    }

    @PostMapping("{employeeId}")
    public Car saveCar(@PathVariable Long employeeId, @RequestBody Car car) {
        return carService.createCar(employeeId, car);
    }

    @PutMapping("{id}")
    public Car editCar(@PathVariable Long id, @RequestBody Car car) {
        return this.carService.editCar(id, car);
    }

    @DeleteMapping("{id}")
    public void deleteCar(@PathVariable Long id) {
        carService.deleteCar(id);
    }
}
