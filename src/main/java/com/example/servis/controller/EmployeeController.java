package com.example.servis.controller;

import com.example.servis.model.Employee;
import com.example.servis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public Iterable<Employee> employeeList() {
        return employeeService.getEmployeeList();
    }

    @GetMapping("{id}")
    public Employee getEmployee(@PathVariable Long id) {
        return employeeService.getEmployee(id);
    }

    @PostMapping("{name}")
    public Employee saveEmployee(@PathVariable String name) {
        return employeeService.saveEmployee(name);
    }

    @DeleteMapping("{id}")
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
    }
}
