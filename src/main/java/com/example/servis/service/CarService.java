package com.example.servis.service;

import com.example.servis.model.Car;

public interface CarService {

    Iterable<Car> carList();

    Car createCar(Long employeeId, Car car);

    Car editCar(Long id, Car car);

    Car getCar(Long id);

    void deleteCar(Long id);

    Iterable<Car> findByCarModel(String carModel);

    Car updateCar(Car car);

    Car getByLicencePlate(String licencePlate);

    Car getCarByDefectId(Long id);
}
