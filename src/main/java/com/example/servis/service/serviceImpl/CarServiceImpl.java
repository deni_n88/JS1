package com.example.servis.service.serviceImpl;

import com.example.servis.model.Car;
import com.example.servis.model.Employee;
import com.example.servis.repository.CarRepository;
import com.example.servis.service.CarService;
import com.example.servis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;
    @Autowired
    private EmployeeService employeeService;

    public Iterable<Car> carList() {
        return carRepository.findAll();
    }

    public Car createCar(Long employeeId, Car car) {
        Employee employee = employeeService.getEmployee(employeeId);
        car.getEmployees().add(employee);
        return carRepository.save(car);
    }

    public Car editCar(Long id, Car car) {
        Car oldCar = carRepository.getOne(id);
        oldCar.setCarModel(car.getCarModel());
        oldCar.setLicencePlate(car.getLicencePlate());
        return carRepository.save(oldCar);
    }

    public Car getCar(Long id) {
        return carRepository.getOne(id);
    }

    public void deleteCar(Long id) {
        carRepository.deleteById(id);
    }

    @Override
    public Iterable<Car> findByCarModel(String carModel) {
        return carRepository.findByCarModelContains(carModel);
    }

    @Override
    public Car updateCar(Car car) {
        return carRepository.save(car);
    }

    @Override
    public Car getByLicencePlate(String licencePlate) {
        return carRepository.getByLicencePlate(licencePlate);
    }

    @Override
    public Car getCarByDefectId(Long id) {
        return carRepository.getCarByDefects(id);
    }

}
