package com.example.servis.service.serviceImpl;

import com.example.servis.model.Employee;
import com.example.servis.repository.EmployeeRepository;
import com.example.servis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee getEmployee(Long id) {
        return employeeRepository.getOne(id);
    }

    public Employee saveEmployee(String name) {
        return employeeRepository.save(new Employee(name));
    }

    public boolean deleteEmployee(Long id) {
        final Employee employeeById = employeeRepository.getOne(id);
        if (employeeById != null) {
            employeeRepository.deleteById(id);
        }
        final Employee removed = employeeRepository.getOne(id);
        if (removed != null) {
            return false;
        }
        return true;
    }

    public Iterable<Employee> getEmployeeList() {
        return employeeRepository.findAll();
    }
}
