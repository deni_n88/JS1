package com.example.servis.service.serviceImpl;

import com.example.servis.model.Car;
import com.example.servis.model.Defect;
import com.example.servis.model.Employee;
import com.example.servis.repository.DefectRepository;
import com.example.servis.service.CarService;
import com.example.servis.service.DefectService;
import com.example.servis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefectServiceImpl implements DefectService {

    @Autowired
    private DefectRepository defectRepository;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private CarService carService;

    public Defect getDefect(Long id) {

        return defectRepository.getOne(id);
    }

    public Iterable<Defect> defectsList() {
        return defectRepository.findAll();
    }

    public Defect saveDefect(Long carId, Long employeeId, Defect defect) {
        Car car = carService.getCar(carId);
        Employee serviceEmployee = employeeService.getEmployee(employeeId);
        Defect newDefect = new Defect(defect.getDetails(), defect.getCost(), car);
        if (car.getEmployees().contains(serviceEmployee)) {
            newDefect.getEmployeeList().add(serviceEmployee);
        } else {
            newDefect.getEmployeeList().add(serviceEmployee);
            car.getEmployees().add(serviceEmployee);
            carService.updateCar(car);
        }
        return defectRepository.save(newDefect);
    }
}
