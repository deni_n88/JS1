package com.example.servis.service;

import com.example.servis.model.Defect;

public interface DefectService {

    Defect getDefect(Long id);

    Iterable<Defect> defectsList();

    Defect saveDefect(Long carId, Long employeeId, Defect defect);

}
