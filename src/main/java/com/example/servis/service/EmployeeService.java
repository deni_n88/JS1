package com.example.servis.service;

import com.example.servis.model.Employee;

import java.util.List;

public interface EmployeeService {

    Employee getEmployee(Long id);

    Employee saveEmployee(String name);

    boolean deleteEmployee(Long id);

    Iterable<Employee> getEmployeeList();
}
