package com.example.servis.config;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CORSFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) resp;

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Allow-Headers",
                "Authorization, Origin, Cache-Control, Content-Type, Accept, X-Requested-With, Access-Control-Allow-Origin");
        response.setHeader("Access-Control-Expose-Headers",
                "Authorization, Content-Type, X-Requested-With, X-Has-Next-Page");
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {
    }
}